package com.example.gpable.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gpable.R;
import com.example.gpable.callbacks.OpenDialogListener;
import com.example.gpable.enums.Add;
import com.example.gpable.holders.AddSemesterHolder;
import com.example.gpable.holders.AddSubjectHolder;
import com.example.gpable.holders.SubjectHolder;
import com.example.gpable.pojos.SubjectPOJO;

import java.util.List;

public class SubjectRecyclerAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<SubjectPOJO> subjectPOJOList;
    private OpenDialogListener openDialogListener;

    public SubjectRecyclerAdapter(Context context,
                                  List<SubjectPOJO> subjectPOJOList,
                                  OpenDialogListener openDialogListener) {
        this.context = context;
        this.subjectPOJOList = subjectPOJOList;
        this.openDialogListener = openDialogListener;
    }

    @Override
    public int getItemViewType(int position) {
        int type = subjectPOJOList.get(position).getItemType();

        if (type == Add.NORMAL.getAddType()) {
            return Add.NORMAL.getAddType();
        } else if (type == Add.ADD_SUBJECT.getAddType()) {
            return Add.ADD_SUBJECT.getAddType();
        } else if (type == Add.ADD_SEMESTER.getAddType()) {
            return Add.ADD_SEMESTER.getAddType();
        }

        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;

        if (i == Add.NORMAL.getAddType()) {
            view = LayoutInflater.from(context).inflate(R.layout.item_subject, viewGroup, false);
            return new SubjectHolder(view);
        } else if (i == Add.ADD_SUBJECT.getAddType()) {
            view = LayoutInflater.from(context).inflate(R.layout.item_add, viewGroup, false);
            return new AddSubjectHolder(view);
        } else if (i == Add.ADD_SEMESTER.getAddType()) {
            view = LayoutInflater.from(context).inflate(R.layout.item_add, viewGroup, false);
            return new AddSemesterHolder(view, openDialogListener);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        int type = getItemViewType(i);

        if (type == Add.NORMAL.getAddType()) {
            SubjectHolder subjectHolder = (SubjectHolder) viewHolder;
            subjectHolder.bind(context, subjectPOJOList.get(i));
        } else if (type == Add.ADD_SUBJECT.getAddType()) {
            AddSubjectHolder addSubjectHolder = (AddSubjectHolder) viewHolder;
            addSubjectHolder.bind(context);
        } else if (type == Add.ADD_SEMESTER.getAddType()) {
            AddSemesterHolder addSemesterHolder = (AddSemesterHolder) viewHolder;
            addSemesterHolder.bind(context);
        }
    }

    @Override
    public int getItemCount() {
        return subjectPOJOList.size();
    }

    public void addSemester(SubjectPOJO subjectPOJO) {
        subjectPOJOList.add(0, subjectPOJO);

    }

    public void addSubject(SubjectPOJO subjectPOJO) {
        subjectPOJOList.add(subjectPOJO);
        notifyItemInserted(subjectPOJOList.size() - 1);
    }
}
