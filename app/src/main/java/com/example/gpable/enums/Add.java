package com.example.gpable.enums;

public enum Add {
    NORMAL(0),
    ADD_SUBJECT(1),
    ADD_SEMESTER(2),
    SUBJECT(3),
    SEMESTER(4);

    private int type;

    private Add(int type) {
        this.type = type;
    }

    public int getAddType() {
        return type;
    }
}
