package com.example.gpable.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.gpable.R;
import com.example.gpable.callbacks.DialogCallBackListener;
import com.example.gpable.enums.Add;
import com.example.gpable.enums.DialogType;
import com.example.gpable.pojos.SubjectPOJO;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddInformationDialog extends AppCompatDialogFragment {
    @BindView(R.id.edit_name)
    EditText editTextName;
    @BindView(R.id.edit_point)
    EditText editTextPoint;
    @BindView(R.id.edit_credit)
    EditText editTextCredit;
    @BindView(R.id.button_submit)
    Button buttonSubmit;

    private DialogType currentDialogTitle;
    private DialogCallBackListener addInformationDialogCallBackListener;

    public static AddInformationDialog newInstance(DialogType dialogType, DialogCallBackListener dialogCallBackListener) {
        AddInformationDialog addInformationDialog = new AddInformationDialog();
        addInformationDialog.currentDialogTitle = dialogType;
        addInformationDialog.addInformationDialogCallBackListener = dialogCallBackListener;
        return addInformationDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.custom_dialog, null);
        ButterKnife.bind(this, view);

        if (currentDialogTitle == DialogType.ADD_SEMESTER) {
            editTextPoint.setVisibility(View.GONE);
            editTextCredit.setVisibility(View.GONE);
        }

        buttonSubmit.setOnClickListener(v -> makeValidations());

        builder.setView(view);
        return builder.create();
    }

    private void makeValidations() {
        SubjectPOJO subjectPOJO = new SubjectPOJO();
        switch (currentDialogTitle) {
            case ADD_SUBJECT:
                // TODO ADD_SUBJECT DIALOG VALIDATION
                subjectPOJO.setItemType(Add.ADD_SUBJECT.getAddType());
                break;
            case ADD_SEMESTER:
                if (editTextName.getText().toString().trim().isEmpty()) {
                    editTextName.setError(getString(R.string.error_enter_new_semester));
                    return;
                }

                subjectPOJO.setItemType(Add.SEMESTER.getAddType());
                break;
        }
        addInformationDialogCallBackListener.onDialogSubmitted(currentDialogTitle, subjectPOJO);
    }
}
