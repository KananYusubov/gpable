package com.example.gpable.ui.activities.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.gpable.R;
import com.example.gpable.adapters.SubjectRecyclerAdapter;
import com.example.gpable.enums.DialogType;
import com.example.gpable.ui.dialogs.AddInformationDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.recycler_subjects)
    RecyclerView recyclerViewSubjects;
    private SubjectRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initRecycler();
    }

    private void initRecycler() {
        adapter = new SubjectRecyclerAdapter(getApplicationContext(),
                new ArrayList<>(),
                x -> openDialog(x));
        recyclerViewSubjects.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerViewSubjects.setAdapter(adapter);
    }

    private void openDialog(DialogType dialogType) {
        AddInformationDialog addInformationDialog = AddInformationDialog.newInstance(dialogType,
                (receivedDialogType, receiverData) -> {
                    if (receivedDialogType == DialogType.ADD_SEMESTER) {

                    } else if (receivedDialogType == DialogType.ADD_SUBJECT) {

                    }
                });
        addInformationDialog.show(getSupportFragmentManager(), getString(R.string.title_new_semester));
    }
}
