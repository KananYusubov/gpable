package com.example.gpable;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
