package com.example.gpable.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.gpable.callbacks.OpenDialogListener;
import com.example.gpable.R;
import com.example.gpable.enums.DialogType;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddSemesterHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text_add)
    TextView textViewAddSemester;
    private OpenDialogListener openDialogListener;

    public AddSemesterHolder(@NonNull View itemView, OpenDialogListener openDialogListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.openDialogListener = openDialogListener;
    }

    public void bind(Context context) {
        textViewAddSemester.setText(context.getString(R.string.add_new_semester));
        itemView.setOnClickListener(v -> openDialogListener.onDialogOpeningRequested(DialogType.ADD_SEMESTER));
    }
}
