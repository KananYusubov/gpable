package com.example.gpable.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.gpable.R;
import com.example.gpable.pojos.SubjectPOJO;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text_subject_name)
    TextView textViewSubjectName;
    @BindView(R.id.text_subject_credit)
    TextView textViewSubjectCredit;
    @BindView(R.id.text_subject_point)
    TextView getTextViewSubjectPoint;

    public SubjectHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Context context, SubjectPOJO subjectPOJO) {
        if (subjectPOJO != null) {
            textViewSubjectName.setText(subjectPOJO.getSubjectTitle());
            textViewSubjectCredit.setText(subjectPOJO.getCredit());
            getTextViewSubjectPoint.setText(subjectPOJO.getPoint());
        }
    }
}
