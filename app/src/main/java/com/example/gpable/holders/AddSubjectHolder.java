package com.example.gpable.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gpable.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddSubjectHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text_add)
    TextView textViewAddSubject;

    public AddSubjectHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Context context) {
        textViewAddSubject.setText(context.getString(R.string.add_new_subject));
        itemView.setOnClickListener(v -> {
            Toast.makeText(context, "Add new subject", Toast.LENGTH_SHORT).show();
        });
    }
}
