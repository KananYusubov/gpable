package com.example.gpable.callbacks;

import com.example.gpable.enums.DialogType;
import com.example.gpable.pojos.SubjectPOJO;

public interface DialogCallBackListener {
    void onDialogSubmitted(DialogType dialogType, SubjectPOJO subjectPOJO);
}
