package com.example.gpable.callbacks;

import com.example.gpable.enums.DialogType;

public interface OpenDialogListener {
    void onDialogOpeningRequested(DialogType dialogType);
}
